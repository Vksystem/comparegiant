<?php
require '../config.php';
require '../include/image.php';
global $table;
$database = new database();
$this_folder = basename(__DIR__);
$module_details = get_module($this_folder);

if (isset($_POST['submit_save']) && $_POST['submit_save'] == "true") {
    $postfield = $_POST;
    unset($postfield['submit_save']);

    $postfield['content_call'] = implode("@|@", array_filter($_POST['content_call']));
    $postfield['content_sms'] = implode("@|@", array_filter($_POST['content_sms']));
    $postfield['content_data'] = implode("@|@", array_filter($_POST['content_data']));

    if ($_FILES['file']['name']) {
        $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        $image = uniqid() . "." . $extension;
        $path = "../../files/" . $module_details['folder'] . "/" . $image;
        move_uploaded_file($_FILES['file']["tmp_name"], $path);
        ImageResize($path, $path, 1200, 1200);
        $postfield['img_url'] = $image;
    }

    $postfield['created_date'] = date('Y-m-d H:i:s');
    $postfield['created_by'] = $user_username;

    $query = get_query_insert($table[$module_details['db_table']], $postfield);
    $result = $database->query($query);
    $genID = $result->insertID();

    foreach ($_POST['category'] as $k => $v) {
        $query = get_query_insert($table['postpaid_to_category'], array('cat_id' => $v, 'product_id' => $genID));
        $database->query($query);
    }

    do_tracking($user_username, 'Add New ' . $module_details['title']);

    header("Location:listing.php?type=new&return=success");
    exit();
}
?>
<!DOCTYPE html>
<html>
<? include('../head.php') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <? include('../header.php') ?>
    <? include('../left.php') ?>

    <div class="content-wrapper">
        <form class="form-horizontal" action="<?= $_SERVER['PHP_SELF'] . "?" . http_build_query($_GET) ?>" method="post"
              enctype="multipart/form-data">

            <section class="content-header">
                <h1>
                    <?= $module_details['title'] ?> > New
                </h1>
                <br>
                <?= get_button($this_folder, 'save', null) . " " . get_button($this_folder, 'cancel', null) ?>
            </section>

            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>

                                    <div class="col-sm-10">
                                        <input type="checkbox" name="status" value="1" checked>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Title</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Description</label>

                                    <div class="col-sm-10">
                                        <textarea name="description" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Monthly Fee</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">RM</span>
                                            <input type="text" class="form-control" name="monthly_fee" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Upfront Fee</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">RM</span>
                                        <input type="text" class="form-control" name="upfront_fee" required>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Data Quota</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="data_quota" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Image</label>

                                    <div class="col-sm-10">
                                        <input type="file" id="file" class="form-control" name="file" required>
                                    </div>
                                </div>
                                <hr>
                                <blockquote>Category</blockquote>
                                <div class="form-group" id="access_div">
                                    <div class="col-sm-12">
                                        <?
                                        $resultCategory = get_query_data($table['postpaid_category']);
                                        while ($rs_category = $resultCategory->fetchRow()) {
                                            ?>
                                            <div class="checkbox">
                                                <label class="control-label">
                                                    <input type="checkbox" name="category[]"
                                                           value="<?= $rs_category['pkid'] ?>">
                                                    <?= $rs_category['title'] ?></label>
                                            </div>
                                        <? } ?>
                                    </div>
                                </div>
                                <hr>
                                <blockquote>Content</blockquote>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Calls</label>
                                    <div id="div_content_call">
                                        <div id="div_content_call_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_call[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row"><i
                                                            class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_call" class="btn btn-success"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">SMS</label>

                                    <div id="div_content_sms">
                                        <div id="div_content_sms_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_sms[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row"><i
                                                            class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_sms" class="btn btn-success"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Data Quota</label>
                                    <div id="div_content_data">
                                        <div id="div_content_data_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_data[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row">
                                                    <i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_data" class="btn btn-success">
                                            <i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <hr>
                                <blockquote>Others</blockquote>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <textarea class="editor" name="content_other"></textarea>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Display Order</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control number" name="sort_order">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?= get_button($this_folder, 'save', null) . " " . get_button($this_folder, 'cancel', null) ?>
            </section>
        </form>
    </div>
</div>
<? include('../js.php') ?>
<script>
    var clone_feature = $("#div_feature_1").clone();
    var clone_petrol = $("#div_content_call_1").clone();
    var clone_grocer = $("#div_content_sms_1").clone();
    var clone_insurance = $("#div_content_data_1").clone();

    $("#file").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 25000
    });

    $(document).on('click', '.remove-row', function () {
        $(this).parent().closest('.div_added').remove();
    });

    $("#button_add_feature").on('click', function () {
        var clone_2 = clone_feature.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_feature").append(clone_2);
    });

    $("#button_add_content_call").on('click', function () {
        var clone_2 = clone_petrol.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_call").append(clone_2);
    });

    $("#button_add_content_sms").on('click', function () {
        var clone_2 = clone_grocer.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_sms").append(clone_2);
    });

    $("#button_add_content_data").on('click', function () {
        var clone_2 = clone_insurance.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_data").append(clone_2);
    });

    $("#button_add_content_utility").on('click', function () {
        var clone_2 = clone_utility.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_utility").append(clone_2);
    });

    $("#button_add_content_shopping").on('click', function () {
        var clone_2 = clone_shopping.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_shopping").append(clone_2);
    });

    $("#button_add_content_entertainment").on('click', function () {
        var clone_2 = clone_entertainment.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_entertainment").append(clone_2);
    });

    $("#button_add_content_travel").on('click', function () {
        var clone_2 = clone_travel.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_travel").append(clone_2);
    });

    $("#button_add_content_dining").on('click', function () {
        var clone_2 = clone_dining.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_dining").append(clone_2);
    });

    $("#button_add_content_other").on('click', function () {
        var clone_2 = clone_other.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_other").append(clone_2);
    });
</script>
</body>
</html>

