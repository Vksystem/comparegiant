<?php
require_once "admin/config.php";
global $table;
$database = new database();

$category_id = mysql_real_escape_string($_GET['sc']);
?>
<!DOCTYPE html>
<html>
<?php include('head.php') ?>

<body>

<div class="header">
    <?php include('header.php') ?>
  <div class="container">
        <h2 class="hero">Contact Us

        </h2>

    </div>
</div>

<div class="container">
    <hr class="lg-space"/>

    <div class="container">
        <div class="row">
            <div class="col-md-6 contact">
                <h3 class="heading">Find Us</h3>
                <hr class="sm-space"/>

                <div class="row">
                    <div class="icon-holder contact-line text-center">
                        <i class="fa fa-map"></i>
                    </div>
                    <div class="col-xs-10 contact-line">
                    <span>
                      123,<br/>
                      Sample Address,<br/>
                      Malaysia.
                    </span>
                    </div>
                </div>

                <div class="row">
                    <div class="icon-holder contact-line text-center">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="col-xs-10 contact-line">
                        <span>+6012 123 4567</span>
                    </div>
                </div>

                <div class="row">
                    <div class="icon-holder contact-line text-center">
                        <i class="fa fa-envelope-o"></i>
                    </div>
                    <div class="col-xs-10 contact-line">
                        <span><a href="mailto:enquiry@comparegiant.com.my">enquiry@comparegiant.com.my</a></span>
                    </div>
                </div>

                <!--<div class="row">
                    <div class="icon-holder contact-line text-center">
                        <i class="ion-social-facebook"></i>
                    </div>
                    <div class="col-xs-10 contact-line">
                        <span><a href="https://www.facebook.com/omegametafengshui" target="_blank">David Xuan</a></span>
                    </div>
                </div>-->
            </div>

            <div class="col-md-6">
                <h3 class="heading">Enquire Now</h3>
                <hr class="sm-space"/>

                <form class="form-horizontal" id="enquiryForm" action="ajax/enquiry.php">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control alpha" name="name" id="inputName" maxlength="100" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputContact" class="col-sm-3 control-label">Contact No.</label>
                        <div class="col-sm-9">
                            <input type="tel" class="form-control number" name="contact" id="inputContact" maxlength="25" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" id="inputEmail" maxlength="150" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputMessage" class="col-sm-3 control-label">Message</label>
                        <div class="col-sm-9">
                            <textarea id="inputMessage" rows="4" name="message" class="form-control" maxlength="250" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-icon"><i class="fa fa-angle-right"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


</body>


<?php include('footer.php') ?>
<?php include('js.php') ?>
</html>
