<?php
require_once "../admin/config.php";
global $table;
$database = new database();

$cat_name = mysql_real_escape_string($_GET['cat']);

switch ($cat_name) {
    case 'credit-card':
        $result = get_query_data($table['credit_card_category'], 'status=1 order by sort_order asc');
        while ($rs_array = $result->fetchRow()) {
            echo '<option value="' . $rs_array['pkid'] . '">' . $rs_array['title'] . '</option>';
        }
        break;

    case 'phone-plan':
        $result = get_query_data($table['phone_device'], 'status=1 order by sort_order asc');
        while ($rs_array = $result->fetchRow()) {
            echo '<option value="' . $rs_array['pkid'] . '">' . $rs_array['title'] . '</option>';
        }
        break;

    case 'postpaid':
        $result = get_query_data($table['postpaid_category'], 'status=1 order by sort_order asc');
        while ($rs_array = $result->fetchRow()) {
            echo '<option value="' . $rs_array['pkid'] . '">' . $rs_array['title'] . '</option>';
        }
        break;

    case 'broadband':
        $result = get_query_data($table['broadband_category'], 'status=1 order by sort_order asc');
        while ($rs_array = $result->fetchRow()) {
            echo '<option value="' . $rs_array['pkid'] . '">' . $rs_array['title'] . '</option>';
        }
        break;

    case 'insurance':
        $result = get_query_data($table['insurance_category'], 'status=1 order by sort_order asc');
        while ($rs_array = $result->fetchRow()) {
            echo '<option value="' . $rs_array['pkid'] . '">' . $rs_array['title'] . '</option>';
        }
        break;
}

exit();
?>