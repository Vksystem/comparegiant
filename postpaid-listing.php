<?php
require_once "admin/config.php";
global $table;
$database = new database();

$category_id = mysql_real_escape_string($_GET['sc']);

if ($_POST['submit_application']) {
    $postfields = $_POST;
    unset($postfields['submit_application']);
    unset($postfields['pdpa']);

    $postfields['created_date'] = date("Y-m-d H:i:s");

    $query = get_query_insert($table['postpaid_enquiry'], $postfields);
    $database->query($query);

    send_email("*",$postfields);

    echo '<script>alert("Successfully Submitted!");</script>';
}

if ($category_id) {
    $resultCat = get_query_data($table['postpaid_category'], "pkid=$category_id");
    $rs_cat = $resultCat->fetchRow();
}else{
    $resultCat = get_query_data($table['postpaid_category'], "status=1 order by sort_order asc");
    $rs_cat = $resultCat->fetchRow();
    $category_id=$rs_cat['pkid'];
}
?>
<!DOCTYPE html>
<html>
<?php include('head.php') ?>

<body>

<div class="header">
    <?php include('header.php') ?>

    <div class="container">
        <h2 class="hero">Best Postpaid Plan in
            <div class="btn-group">
                <button type="button" class="btn-link inline-dropdown dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <label><?= $rs_cat['title'] ?></label> <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu">
                    <?
                    $resultSubCat = get_query_data($table['postpaid_category'], 'status=1 order by sort_order asc');
                    while ($rs_subcat = $resultSubCat->fetchRow()) {
                        echo '<li><a href="postpaid-listing.php?sc=' . $rs_subcat['pkid'] . '">' . $rs_subcat['title'] . '</a></li>';
                    }
                    ?>
                </ul>
            </div>
        </h2>

        <p>
            Find the credit card that best meets your needs, or simply compare all credit cards from your favorite bank.
            Click Apply to proceed with the credit card application online, it's easy and convenient.
        </p>
    </div>
</div>

<div class="container">
    <hr class="lg-space"/>
    <div class="listing">

        <?
        if ($category_id != "") {
            $resultCard = get_query_data($table['postpaid'], "status=1 and pkid in (select product_id from " . $table['postpaid_to_category'] . " where cat_id=$category_id) order by sort_order asc");
        } else {
            $resultCard = get_query_data($table['postpaid'], "status=1 order by sort_order asc");
        }
        $row_card = $resultCard->numRows();
        if ($row_card == 0) {
            echo '<h4>No data found.</h4>';
        }
        while ($rs_card = $resultCard->fetchRow()) {
            ?>
            <div class="item-card">
                <div class="row">
                    <a href="postpaid-inner.php?id=<?= $rs_card['pkid'] ?>">
                        <div class="col-md-2 col-xs-12 prod-img">
                            <div class="img-holder">
                                <img src="files/postpaid/<?= $rs_card['img_url'] ?>"
                                />
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12 main-intro">
                            <h2><?= $rs_card['title'] ?></h2>
                            <div class="desc">
                                <?= $rs_card['description'] ?>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-4 main-feature">
                            <label>Monthly Fee</label>
                            <figure>
                                RM <?= number_format($rs_card['monthly_fee']) ?>
                            </figure>
                        </div>
                        <div class="col-md-2 col-xs-4 main-feature">
                            <label>Upfront Fee</label>
                            <figure>
                                RM <?= number_format($rs_card['upfront_fee']) ?>
                            </figure>
                        </div>
                        <div class="col-md-2 col-xs-4 main-feature">
                            <label>Data Quota</label>
                            <figure>
                                <?= $rs_card['data_quota']?>
                            </figure>
                        </div>
                    </a>
                    <div class="col-md-1 col-xs-12 main-cta" onclick="modal_application('<?= $rs_card['title'] ?>')">
                        <label>Apply Now</label>
                    </div>
                </div>
            </div>
        <? } ?>
    </div>
</div>


</body>


<?php include('footer.php') ?>
<?php include('js.php') ?>
</html>
