<?php
require '../config.php';
global $table;
$database = new database();
$this_folder = basename(__DIR__);

if (isset($_POST['submit_save']) && $_POST['submit_save'] == "true") {
    $postfield = $_POST;
    unset($postfield['submit_save']);
    unset($postfield['access']);

    $postfield['password'] = protect('encrypt', $postfield['password']);
    $postfield['created_date'] = date('Y-m-d H:i:s');
    $postfield['created_by'] = $user_username;

    $query = get_query_insert($table['user'], $postfield);
    $result = $database->query($query);
    $genID = $result->insertID();

        foreach ($_POST['access'] as $k => $v) {
            $query = get_query_insert($table['user_access'], array('user_id' => $genID, 'module_id' => $v));
            $database->query($query);
        }

    do_tracking($user_username, 'Add New User');

    header("Location:listing.php?type=new&return=success");
    exit();
}
?>
<!DOCTYPE html>
<html>
<? include('../head.php') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <? include('../header.php') ?>
    <? include('../left.php') ?>

    <div class="content-wrapper">
        <form class="form-horizontal" action="<?= $_SERVER['PHP_SELF'] . "?" . http_build_query($_GET) ?>" method="post"
              enctype="multipart/form-data">

            <section class="content-header">
                <h1>
                    Users > New
                </h1>
                <br>
                <?= get_button($this_folder, 'save', null) . " " . get_button($this_folder, 'cancel', null) ?>
            </section>

            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>

                                    <div class="col-sm-10">
                                        <input type="checkbox" name="status" value="1" checked>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Name</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Username</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="username" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Password</label>

                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Role</label>

                                    <div class="col-sm-10">
                                        <select name="role" class="form-control">
                                            <option value="1">Admin</option>
                                            <option value="2">User</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="access_div">
                                    <label class="col-sm-2 control-label">Access</label>

                                    <div class="col-sm-10">
                                        <?
                                        $resultModule = get_query_data($table['module'], "1 order by sort_order asc");
                                        while ($rs_module = $resultModule->fetchRow()) {
                                            ?>
                                            <div class="checkbox">
                                                <label class="control-label">
                                                    <input type="checkbox" name="access[]"
                                                           value="<?= $rs_module['pkid'] ?>">
                                                    <?= $rs_module['title'] ?></label>
                                            </div>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?= get_button($this_folder, 'save', null) . " " . get_button($this_folder, 'cancel', null) ?>
            </section>
        </form>
    </div>
</div>
<? include('../js.php') ?>
<script>
    $("#access_div").hide();

    $("#file").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 25000
    });

    $("select[name='role']").on('change', function () {
        if (this.value == "2") {
            $("#access_div").show(500);
        } else {
            $("#access_div").hide(500);
        }
    });
</script>
</body>
</html>

