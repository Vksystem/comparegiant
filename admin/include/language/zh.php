<?
//menu
$lang['home']="首页";
$lang['about_us']="关于我们";
$lang['our_company']="公司简介";
$lang['our_project']="我们的业务";
$lang['gallery']="相册";
$lang['news_&_event']="资讯与活动";
$lang['contact']="联系我们";
$lang['member_login']="会员登录";

//index
$lang['our_profile']="简介";

//footer
$lang['address']="地址";
$lang['email']="电邮";
$lang['follow_us']="关注我们";
$lang['phone']="联络号码";
$lang['copy_right']="© 2016 EcoGreen Holding Sdn Bhd. 版权所有";

//contact
$lang['find_us']="办公地点";
$lang['drop_us_a_message']="联系我们";
$lang['name']="姓名";
$lang['enquiry']="疑问";

//project
$lang['about_the_project']="关于这个项目";

//general
$lang['back_to']="返回";
$lang['close']="关闭";
$lang['read_more']="更多";
$lang['submit']="提交";
$lang['error_404']="错误 404";
$lang['page_not_found']="页面无法访问";
$lang['404_content']="您查看的页面不存在或已被删除";
?>