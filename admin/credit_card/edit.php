<?php
require '../config.php';
require '../include/image.php';
global $table;
$database = new database();
$this_folder = basename(__DIR__);
$module_details = get_module($this_folder);
$pkid = mysql_real_escape_string($_GET['id']);

$query = "select * from " . $table[$module_details['db_table']] . " where pkid=$pkid";
$result = $database->query($query);
$rs_array = $result->fetchRow();

if (isset($_POST['submit_save']) && $_POST['submit_save'] == "true") {
    $postfield = $_POST;
    unset($postfield['submit_save']);
    unset($postfield['category']);

    $postfield['feature']=implode("@|@",array_filter($_POST['feature']));
    $postfield['content_petrol']=implode("@|@",array_filter($_POST['content_petrol']));
    $postfield['content_grocer']=implode("@|@",array_filter($_POST['content_grocer']));
    $postfield['content_insurance']=implode("@|@",array_filter($_POST['content_insurance']));
    $postfield['content_utility']=implode("@|@",array_filter($_POST['content_utility']));
    $postfield['content_shopping']=implode("@|@",array_filter($_POST['content_shopping']));
    $postfield['content_entertainment']=implode("@|@",array_filter($_POST['content_entertainment']));
    $postfield['content_travel']=implode("@|@",array_filter($_POST['content_travel']));
    $postfield['content_dining']=implode("@|@",array_filter($_POST['content_dining']));
    $postfield['content_other']=implode("@|@",array_filter($_POST['content_other']));

    if ($_FILES['file']['name']) {
        $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        $image = uniqid() . "." . $extension;
        $path = "../../files/" . $module_details['folder'] . "/" . $image;
        move_uploaded_file($_FILES['file']["tmp_name"], $path);
        ImageResize($path, $path, 1200, 1200);
        $postfield['img_url'] = $image;
    }

    $postfield['status'] = $_POST['status'];
    $postfield['updated_date'] = date('Y-m-d H:i:s');
    $postfield['updated_by'] = $user_username;

    $query = get_query_update($table[$module_details['db_table']], $pkid, $postfield);
    $database->query($query);

    $query = get_query_delete_all($table['credit_card_to_category'], "credit_card_id=$pkid");
    $database->query($query);

    foreach ($_POST['category'] as $k => $v) {
        $query = get_query_insert($table['credit_card_to_category'], array('cat_id' => $v, 'credit_card_id' => $pkid));
        $database->query($query);
    }

    do_tracking($user_username, 'Add New ' . $module_details['title']);

    header("Location:listing.php?type=new&return=success");
    exit();
}
?>
<!DOCTYPE html>
<html>
<? include('../head.php') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <? include('../header.php') ?>
    <? include('../left.php') ?>

    <div class="content-wrapper">
        <form class="form-horizontal" action="<?= $_SERVER['PHP_SELF'] . "?" . http_build_query($_GET) ?>" method="post"
              enctype="multipart/form-data">

            <section class="content-header">
                <h1>
                    <?= $module_details['title'] ?> > Edit
                </h1>
                <br>
                <?= get_button($this_folder, 'save', null) . " " . get_button($this_folder, 'cancel', null) ?>
            </section>

            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>

                                    <div class="col-sm-10">
                                        <input type="checkbox" name="status"
                                               value="1" <?= $rs_array['status'] == "1" ? "checked" : "" ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Title</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title" required
                                               value="<?= $rs_array['title'] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Description</label>

                                    <div class="col-sm-10">
                                        <textarea name="description" class="form-control"
                                                  rows="3"><?= $rs_array['description'] ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Image</label>

                                    <div class="col-sm-10">
                                        <input type="file" id="file" class="form-control" name="file">
                                    </div>
                                </div>
                                <hr>
                                <blockquote>Category</blockquote>
                                <div class="form-group" id="access_div">
                                    <div class="col-sm-12">
                                        <?
                                        $resultCategory = get_query_data($table['credit_card_to_category'], "1 and credit_card_id=$pkid");
                                        while ($rs_category = $resultCategory->fetchRow()) {
                                            $array_category[] = $rs_category['cat_id'];
                                        }

                                        $resultCategory = get_query_data($table['credit_card_category']);
                                        while ($rs_category = $resultCategory->fetchRow()) {
                                            ?>
                                            <div class="checkbox">
                                                <label class="control-label">
                                                    <input type="checkbox" name="category[]"
                                                           value="<?= $rs_category['pkid'] ?>" <?= in_array($rs_category['pkid'], $array_category) ? "checked" : "" ?>>
                                                    <?= $rs_category['title'] ?></label>
                                            </div>
                                        <? } ?>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Min. Monthly Income</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="income" required
                                               value="<?= $rs_array['income'] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Interest Rate</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="interest" required
                                               value="<?= $rs_array['interest'] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Balance Transfer</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="balance" required
                                               value="<?= $rs_array['balance'] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Annual Fee</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="annual" required
                                               value="<?= $rs_array['annual'] ?>">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Feature Highlight</label>
                                    <div id="div_feature">
                                        <div id="div_feature_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="feature[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row"><i
                                                            class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                    $array_feature = explode("@|@", $rs_array['feature']);
                                    $array_feature = array_filter($array_feature);

                                    foreach ($array_feature as $k => $v) {
                                        ?>
                                        <div class="div_added">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <input type="text" class="form-control" name="feature[]"
                                                       value="<?= $v ?>">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row"><i
                                                            class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                    <? } ?>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_feature" class="btn btn-success"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <hr>
                                <blockquote>Rating</blockquote>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Petrol</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="rating_petrol" value="<?=$rs_array['rating_petrol']?>">
                                            <span class="input-group-addon">/10</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Groceries</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="rating_grocer" value="<?=$rs_array['rating_grocer']?>">
                                            <span class="input-group-addon">/10</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Insurance / Health</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="rating_insurance" value="<?=$rs_array['rating_insurance']?>">
                                            <span class="input-group-addon">/10</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Utilities</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="rating_utility" value="<?=$rs_array['rating_utility']?>">
                                            <span class="input-group-addon">/10</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Shopping</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="rating_shopping" value="<?=$rs_array['rating_shopping']?>">
                                            <span class="input-group-addon">/10</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Entertainment</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="rating_entertainment" value="<?=$rs_array['rating_entertainment']?>">
                                            <span class="input-group-addon">/10</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Travel</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="rating_travel" value="<?=$rs_array['rating_travel']?>">
                                            <span class="input-group-addon">/10</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Dining</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="rating_dining" value="<?=$rs_array['rating_dining']?>">
                                            <span class="input-group-addon">/10</span>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <blockquote>Content</blockquote>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Petrol</label>
                                    <div id="div_content_petrol">
                                        <div id="div_content_petrol_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_petrol[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row"><i
                                                            class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <?
                                    $array_feature = explode("@|@", $rs_array['content_petrol']);
                                    $array_feature = array_filter($array_feature);

                                    foreach ($array_feature as $k => $v) {
                                        ?>
                                        <div class="div_added">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <input type="text" class="form-control" name="content_petrol[]"
                                                       value="<?= $v ?>">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row"><i
                                                            class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                    <? } ?>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_petrol" class="btn btn-success"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Groceries</label>

                                    <div id="div_content_grocer">
                                        <div id="div_content_grocer_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_grocer[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row"><i
                                                            class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <?
                                        $array_feature = explode("@|@", $rs_array['content_grocer']);
                                        $array_feature = array_filter($array_feature);

                                        foreach ($array_feature as $k => $v) {
                                            ?>
                                            <div class="div_added">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <input type="text" class="form-control" name="content_grocer[]"
                                                           value="<?= $v ?>">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button"
                                                            class="btn btn-danger remove-row"><i
                                                                class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                        <? } ?>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_grocer" class="btn btn-success"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Insurance / Health</label>
                                    <div id="div_content_insurance">
                                        <div id="div_content_insurance_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_insurance[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row">
                                                    <i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <?
                                        $array_feature = explode("@|@", $rs_array['content_insurance']);
                                        $array_feature = array_filter($array_feature);

                                        foreach ($array_feature as $k => $v) {
                                            ?>
                                            <div class="div_added">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <input type="text" class="form-control" name="content_insurance[]"
                                                           value="<?= $v ?>">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button"
                                                            class="btn btn-danger remove-row"><i
                                                                class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                        <? } ?>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_insurance" class="btn btn-success">
                                            <i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Utilities</label>
                                    <div id="div_content_utility">
                                        <div id="div_content_utility_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_utility[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row"><i
                                                            class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <?
                                        $array_feature = explode("@|@", $rs_array['content_utility']);
                                        $array_feature = array_filter($array_feature);

                                        foreach ($array_feature as $k => $v) {
                                            ?>
                                            <div class="div_added">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <input type="text" class="form-control" name="content_utility[]"
                                                           value="<?= $v ?>">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button"
                                                            class="btn btn-danger remove-row"><i
                                                                class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                        <? } ?>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_utility" class="btn btn-success"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Shopping</label>
                                    <div id="div_content_shopping">
                                        <div id="div_content_shopping_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_shopping[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row">
                                                    <i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <?
                                        $array_feature = explode("@|@", $rs_array['content_shopping']);
                                        $array_feature = array_filter($array_feature);

                                        foreach ($array_feature as $k => $v) {
                                            ?>
                                            <div class="div_added">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <input type="text" class="form-control" name="content_shopping[]"
                                                           value="<?= $v ?>">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button"
                                                            class="btn btn-danger remove-row"><i
                                                                class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                        <? } ?>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_shopping" class="btn btn-success">
                                            <i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Entertainment</label>
                                    <div id="div_content_entertainment">
                                        <div id="div_content_entertainment_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_entertainment[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row">
                                                    <i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <?
                                        $array_feature = explode("@|@", $rs_array['content_entertainment']);
                                        $array_feature = array_filter($array_feature);

                                        foreach ($array_feature as $k => $v) {
                                            ?>
                                            <div class="div_added">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <input type="text" class="form-control" name="content_entertainment[]"
                                                           value="<?= $v ?>">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button"
                                                            class="btn btn-danger remove-row"><i
                                                                class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                        <? } ?>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_entertainment"
                                                class="btn btn-success"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Travel</label>
                                    <div id="div_content_travel">
                                        <div id="div_content_travel_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_travel[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row">
                                                    <i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <?
                                        $array_feature = explode("@|@", $rs_array['content_travel']);
                                        $array_feature = array_filter($array_feature);

                                        foreach ($array_feature as $k => $v) {
                                            ?>
                                            <div class="div_added">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <input type="text" class="form-control" name="content_travel[]"
                                                           value="<?= $v ?>">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button"
                                                            class="btn btn-danger remove-row"><i
                                                                class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                        <? } ?>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_travel" class="btn btn-success"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Dining</label>
                                    <div id="div_content_dining">
                                        <div id="div_content_dining_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_dining[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row">
                                                    <i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <?
                                        $array_feature = explode("@|@", $rs_array['content_dining']);
                                        $array_feature = array_filter($array_feature);

                                        foreach ($array_feature as $k => $v) {
                                            ?>
                                            <div class="div_added">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <input type="text" class="form-control" name="content_dining[]"
                                                           value="<?= $v ?>">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button"
                                                            class="btn btn-danger remove-row"><i
                                                                class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                        <? } ?>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_dining" class="btn btn-success"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Other Features</label>
                                    <div id="div_content_other">
                                        <div id="div_content_other_1">
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="content_other[]">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button"
                                                        class="btn btn-danger remove-row">
                                                    <i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <?
                                        $array_feature = explode("@|@", $rs_array['content_other']);
                                        $array_feature = array_filter($array_feature);

                                        foreach ($array_feature as $k => $v) {
                                            ?>
                                            <div class="div_added">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <input type="text" class="form-control" name="content_other[]"
                                                           value="<?= $v ?>">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button"
                                                            class="btn btn-danger remove-row"><i
                                                                class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                        <? } ?>
                                    </div>
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button type="button" id="button_add_content_other" class="btn btn-success"><i
                                                    class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <hr>
                                <blockquote>Card Information</blockquote>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Min. age for principal holder</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_min_age1" value="<?=$rs_array['info_min_age1']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Min. age for supplementary holder</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_min_age2" value="<?=$rs_array['info_min_age2']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Fee for supplementary holder</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_fee" value="<?=$rs_array['info_fee']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Finance charges</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_finance1" value="<?=$rs_array['info_finance1']?>">
                                        <input type="text" class="form-control" name="info_finance2" value="<?=$rs_array['info_finance2']?>">
                                        <input type="text" class="form-control" name="info_finance3" value="<?=$rs_array['info_finance3']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Goods & Services Tax (GST)</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_gst" value="<?=$rs_array['info_gst']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Late payment charge</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_late_payment" value="<?=$rs_array['info_late_payment']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cash advance amount</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_cash_advanced_amount" value="<?=$rs_array['info_cash_advanced_amount']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cash advance charge</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_cash_advanced_charge" value="<?=$rs_array['info_cash_advanced_charge']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cash advance interest charge</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_cash_advanced_interest" value="<?=$rs_array['info_cash_advanced_interest']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cashback Limit</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_cashback" value="<?=$rs_array['info_cashback']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Rewards points expiry</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="info_reward_point" value="<?=$rs_array['info_reward_point']?>">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Display Order</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control number" name="sort_order" value="<?=$rs_array['sort_order']?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?= get_button($this_folder, 'save', null) . " " . get_button($this_folder, 'cancel', null) ?>
            </section>

        </form>
    </div>
</div>
<? include('../js.php') ?>
<script>
    $("#file").fileinput({
        showRemove: false,
        showUpload: false,
        showCancel: false,
        maxFileCount: 1,
        maxFileSize: 25000,
        <?if($rs_array['img_url'] != ""){?>
        initialPreview: [
            "<img src='../files/<?=$module_details['folder']?>/<?=$rs_array['img_url']?>' class='file-preview-image img-responsive'>"
        ],
        <?}?>
    });

    var clone_feature = $("#div_feature_1").clone();
    var clone_petrol = $("#div_content_petrol_1").clone();
    var clone_grocer = $("#div_content_grocer_1").clone();
    var clone_insurance = $("#div_content_insurance_1").clone();
    var clone_utility = $("#div_content_utility_1").clone();
    var clone_shopping = $("#div_content_shopping_1").clone();
    var clone_entertainment = $("#div_content_entertainment_1").clone();
    var clone_travel = $("#div_content_travel_1").clone();
    var clone_dining = $("#div_content_dining_1").clone();
    var clone_other = $("#div_content_other_1").clone();

    $(document).on('click', '.remove-row', function () {
        $(this).parent().closest('.div_added').remove();
    });

    $("#button_add_feature").on('click', function () {
        var clone_2 = clone_feature.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_feature").append(clone_2);
    });

    $("#button_add_content_petrol").on('click', function () {
        var clone_2 = clone_petrol.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_petrol").append(clone_2);
    });

    $("#button_add_content_grocer").on('click', function () {
        var clone_2 = clone_grocer.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_grocer").append(clone_2);
    });

    $("#button_add_content_insurance").on('click', function () {
        var clone_2 = clone_insurance.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_insurance").append(clone_2);
    });

    $("#button_add_content_utility").on('click', function () {
        var clone_2 = clone_utility.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_utility").append(clone_2);
    });

    $("#button_add_content_shopping").on('click', function () {
        var clone_2 = clone_shopping.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_shopping").append(clone_2);
    });

    $("#button_add_content_entertainment").on('click', function () {
        var clone_2 = clone_entertainment.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_entertainment").append(clone_2);
    });

    $("#button_add_content_travel").on('click', function () {
        var clone_2 = clone_travel.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_travel").append(clone_2);
    });

    $("#button_add_content_dining").on('click', function () {
        var clone_2 = clone_dining.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_dining").append(clone_2);
    });

    $("#button_add_content_other").on('click', function () {
        var clone_2 = clone_other.clone();
        clone_2.prop('id', '').prop('class', 'div_added');
        clone_2.find('input[type=text]').val('').end();
        clone_2.find('div.col-sm-8').attr('class', 'col-sm-8 col-sm-offset-2');
        clone_2.find('button').attr('id', '').end();
        clone_2.find('button').attr('class', 'btn btn-danger remove-row').end();
        clone_2.find('i').attr('class', 'fa fa-minus').end();
        $("#div_content_other").append(clone_2);
    });
</script>
</body>
</html>

