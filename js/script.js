// Remove transition on page load
$(window).load(function() {
  $("body").removeClass("preload");
});

// Smooth Scroll
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

// match height
$(document).ready(function(){
  $('.feature-listing').matchHeight({});

  /*$( "header nav > ul" ).clone().appendTo( ".overlay nav" );*/
});

// Bootstrap select
$('.selectpicker').selectpicker({
  style: 'form-select',
});

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
    $('.selectpicker').selectpicker({
      mobile: true
    });
}

// Radio
$(document).ready(function() {
    $('input[name="search-category"]').click(function () {
        $('input:not(:checked)').parent().removeClass("checked");
        $('input:checked').parent().addClass("checked");
    });
});


// match height for md
/*$(document).ready(function(){
   var eventFired = 0;

  if ($(window).width() > 991) {
      $('.item-card .prod-img, .item-card .main-cta, .item-card .main-feature').matchHeight({target: $('.item-card')});

  } else {
    $('.item-card .prod-img, .item-card .main-cta, .item-card .main-feature').matchHeight({remove: true});
  }

  $(window).on('resize', function() {
      if (!eventFired) {
          if ($(window).width() > 991) {
              $('.item-card .prod-img, .item-card .main-cta, .item-card .main-feature').matchHeight({target: $('.item-card')});
          } else {
              $('.item-card .prod-img, .item-card .main-cta, .item-card .main-feature').matchHeight({remove: true});
          }
      }
  });
});*/
