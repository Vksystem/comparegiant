<?php
require_once "admin/config.php";
global $table;
$database = new database();

$category_id = mysql_real_escape_string($_GET['sc']);
?>
<!DOCTYPE html>
<html>
<?php include('head.php') ?>

<body>

<div class="header">
    <?php include('header.php') ?>
  <div class="container">
        <h2 class="hero">Best Smart Phone Plans

        </h2>

        <p>
          Stay connected with the best iPhone, Android, Blackberry or Windows Phone. Compare phone prices and bundle it together with a mobile plan with unlimited internet for faster 3G and 4G LTE.

        </p>
    </div>
</div>

<div class="container">
    <hr class="lg-space"/>
    <div class="listing">

        <?
        $resultCard = get_query_data($table['phone_device'], "status=1 order by sort_order asc");
        $row_card = $resultCard->numRows();
        if ($row_card == 0) {
            echo '<h4>No data found.</h4>';
        }
        while ($rs_card = $resultCard->fetchRow()) {
            ?>
            <div class="item-card">
                <div class="row">
                    <a href="phone-plan-listing.php?id=<?= $rs_card['pkid'] ?>">
                        <div class="col-md-2 col-xs-12 prod-img">
                            <div class="img-holder">
                                <img src="files/phone_device/<?= $rs_card['img_url'] ?>"
                                />
                            </div>
                        </div>
                        <div class="col-md-5 col-xs-12 main-intro">
                            <h2><?= $rs_card['title'] ?></h2>
                            <div class="desc">
                                <?= $rs_card['description'] ?>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-4 main-feature">
                            <label>Capacity</label>
                            <figure>
                                <?= $rs_card['capacity'] ?>
                            </figure>
                        </div>
                        <div class="col-md-2 col-xs-4 main-feature">
                            <label>Retail Price</label>
                            <figure>
                                RM <?= number_format($rs_card['price']) ?>
                            </figure>
                        </div>
                        <div class="col-md-1 col-xs-12 main-cta">
                            <label>View Plan</label>
                        </div>
                    </a>
                </div>
            </div>
        <? } ?>
    </div>
</div>


</body>


<?php include('footer.php') ?>
<?php include('js.php') ?>
</html>
