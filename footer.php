<section id="introduction">
    <div class="container text-center">
        <h2>CompareGiant</h2>
        <p>
            BolehCompare.com ("we") doesn't provide any financial or professional advice. We only provide a free and
            impartial price comparison service. We are neither liable nor responsible for any inaccurate information
            that may be viewed on this website. In case of any discrepancy between the information provided by
            BolehCompare.com and relevant information provided by banks, card issuers or card providers ("partner
            institutions"), the information provided by the partner institutions shall prevail. Please read carefully
            our terms and conditions and privacy policy for more information.
        </p>
    </div>
</section>

<section id="footer">
    <div class="container text-center">
        <ul class="list-unstyled list-inline">
            <li><a href="about.php">About Us</a></li>
            <li><a href="contact.php">Contact Us</a></li>
            <li><a href="terms.php">Terms & Conditions</a></li>
        </ul>

        <div class="copyright">
            © 2017 CompareGiant. All rights reserved.
        </div>
    </div>
</section>

<!-- Credit card Modal -->
<div class="modal fade" id="credit-card-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form action="<?= $_SERVER['PHP_SELF'] . "?" . http_build_query($_GET) ?>" method="post">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Apply Now for <span id="credit-card-modal_title"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Full name as per IC"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="example@email.com"
                               required>
                    </div>

                    <div class="form-group">
                        <label for="contact">Mobile Number</label>
                        <input type="tel" class="form-control" id="contact" name="mobile" placeholder="" required>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="pdpa" required> You allow us to pass on your information to
                            product providers and
                            accept our Privacy Policy. We follow the Personal Data Protection Act (PDPA).
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="product_name" id="product_name">
                    <button type="button" class="btn btn-grey" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="submit_application" value="true" class="btn btn-default">Apply</button>
                </div>
            </div>
        </div>
    </form>
</div>



<!--mobile nav-->
<div class="overlay overlay-corner">
    <button type="button" class="overlay-close">Close</button>
    <nav>
      <ul>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Credit Card <i class="fa fa-angle-down"></i></a>
          <ul class="dropdown-menu">
              <?
            $resultSubCat = get_query_data($table['credit_card_category'], 'status=1 order by sort_order asc');
            while ($rs_subcat = $resultSubCat->fetchRow()) {
                echo '<li class="sub-menu"><a href="credit-card-listing.php?sc='.$rs_subcat['pkid'].'">'.$rs_subcat['title'].'</a></li>';
            }
            ?>
          </ul>
        </li>

        <li>
          <a href="broadband-listing.php">Broadband</a>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mobile <i class="fa fa-angle-down"></i></a>
          <ul class="dropdown-menu">
            <li class="sub-menu"><a href="phone-listing.php">Smart Phone Plan</a></li>
            <li class="sub-menu"><a href="postpaid-listing.php">Postpaid Plan</a></li>
          </ul>
        </li>

        <!--<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insurance <i class="fa fa-angle-down"></i></a>
          <ul class="dropdown-menu">
            <li class="sub-menu"><a href="listing.php">Medical Card</a></li>
            <li class="sub-menu"><a href="listing.php">Life Insurance</a></li>
          </ul>
        </li>-->

        <li>
          <a href="contact.php">Contact Us</a>
        </li>
      </ul>
    </nav>
</div>
