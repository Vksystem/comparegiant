<?php
require_once "admin/config.php";
global $table;
$database = new database();

$device_id = mysql_real_escape_string($_GET['id']);

if ($_POST['submit_application']) {
    $postfields = $_POST;
    unset($postfields['submit_application']);
    unset($postfields['pdpa']);

    $postfields['created_date']=date("Y-m-d H:i:s");

    $query=get_query_insert($table['phone_enquiry'],$postfields);
    $database->query($query);

    send_email("*",$postfields);

    echo '<script>alert("Successfully Submitted!");</script>';
}

$resultDevice=get_query_data($table['phone_device'],"pkid=$device_id");
$rs_device=$resultDevice->fetchRow();
?>
<!DOCTYPE html>
<html>
<?php include('head.php') ?>

<body>

<div class="header">
    <?php include('header.php') ?>
    <div class="container">
        <h2 class="hero">Best <?=$rs_device['title']?> Smart Phone Plan
        </h2>
        <p>
           Get unlimited data plan and the best <?=$rs_device['title']?> price in Malaysia in a single deal!
            Enjoy super fast 4G LTE for more downloads from the app store including <?=$rs_device['title']?> wallpaper and ringtones.
            Choose your favourite <?=$rs_device['title']?> colours and specifications.
        </p>
    </div>
</div>

<div class="container">
    <hr class="lg-space"/>
    <div class="listing">

        <?
        $resultCard = get_query_data($table['phone_plan'], "status=1 and pkid in (select plan_id from ".$table['device_to_plan']." where device_id=$device_id) order by sort_order asc");
        $row_card = $resultCard->numRows();
        if ($row_card == 0) {
            echo '<h4>No data found.</h4>';
        }
        while ($rs_card = $resultCard->fetchRow()) {
            $resultPrice=get_query_data($table['device_to_plan'],"device_id=$device_id and plan_id=".$rs_card['pkid']);
            $rs_price=$resultPrice->fetchRow();
            ?>
            <div class="item-card">
                <div class="row">
                    <a href="phone-plan-inner.php?id=<?= $rs_card['pkid'] ?>&did=<?=$device_id?>">
                        <div class="col-md-2 col-xs-12 prod-img">
                            <div class="img-holder">
                                <img src="files/phone_plan/<?= $rs_card['img_url'] ?>"
                                />
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12 main-intro">
                            <h2><?= $rs_card['title'] ?></h2>
                            <div class="desc">
                                <?= $rs_card['description'] ?>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-4 main-feature">
                            <label>Monthly Fee</label>
                            <figure>
                                RM <?= number_format($rs_card['monthly_fee']) ?>
                            </figure>
                        </div>
                        <div class="col-md-2 col-xs-4 main-feature">
                            <label>Data Quota</label>
                            <figure>
                                <?= $rs_card['data_quota'] ?>
                            </figure>
                        </div>
                        <div class="col-md-2 col-xs-4 main-feature">
                            <label>Device Price</label>
                            <figure>
                               RM <?= number_format($rs_price['price']) ?>
                            </figure>
                        </div>
                    </a>
                    <div class="col-md-1 col-xs-12 main-cta" onclick="modal_application('<?=$rs_card['title']." & ".$rs_device['title']?>')">
                            <label>Apply Now</label>
                        </div>
                </div>
            </div>
        <? } ?>
    </div>
</div>


</body>


<?php include('footer.php') ?>
<?php include('js.php') ?>
</html>
