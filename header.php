<header>
        <div class="cbp-af-header">
  				<div class="cbp-af-inner container">
            <a href="index.php">
  					  <img src="img/logo-bk.png" class="logo logo-colored" alt="CompareGiant" style="width: 240px;"/>
            </a>
  					<nav>
              <ul>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Credit Card <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu">
                      <?
                    $resultSubCat = get_query_data($table['credit_card_category'], 'status=1 order by sort_order asc');
                    while ($rs_subcat = $resultSubCat->fetchRow()) {
                        echo '<li class="sub-menu"><a href="credit-card-listing.php?sc='.$rs_subcat['pkid'].'">'.$rs_subcat['title'].'</a></li>';
                    }
                    ?>
                  </ul>
                </li>

                <li>
                  <a href="broadband-listing.php">Broadband</a>
                </li>

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mobile <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu">
                    <li class="sub-menu"><a href="phone-listing.php">Smart Phone Plan</a></li>
                    <li class="sub-menu"><a href="postpaid-listing.php">Postpaid Plan</a></li>
                  </ul>
                </li>

                <!--<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insurance <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu">
                    <li class="sub-menu"><a href="listing.php">Medical Card</a></li>
                    <li class="sub-menu"><a href="listing.php">Life Insurance</a></li>
                  </ul>
                </li>-->

                <li>
                  <a href="contact.php">Contact Us</a>
                </li>
              </ul>
  					</nav>
            <div class="hamburger" id="trigger-overlay" role="button">
              <i class="lnr lnr-menu"></i>
            </div>
  				</div>
  			</div>
      </header>
