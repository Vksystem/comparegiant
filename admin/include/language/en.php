<?
//menu
$lang['home']="Home";
$lang['about_us']="About Us";
$lang['our_company']="Our Company";
$lang['our_project']="Our Projects";
$lang['gallery']="Gallery";
$lang['news_&_event']="News & Events";
$lang['contact']="Contact";
$lang['member_login']="Member Login";

//index
$lang['our_profile']="Our Profile";

//footer
$lang['address']="Address";
$lang['email']="Email";
$lang['follow_us']="Follow Us";
$lang['phone']="Contact";
$lang['copy_right']="© 2016 EcoGreen Holding Sdn Bhd. All Rights Reserved.";

//contact
$lang['find_us']="Find Us";
$lang['drop_us_a_message']="Drop Us A Message";
$lang['name']="Name";
$lang['enquiry']="Enquiry";

//project
$lang['about_the_project']="About the Project";

//general
$lang['back_to']="Back to";
$lang['close']="Close";
$lang['read_more']="Read More";
$lang['submit']="Submit";
$lang['error_404']="Error 404";
$lang['page_not_found']="Page not found";
$lang['404_content']="The page you are looking for something that isn't here.";
?>