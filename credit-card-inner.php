<?php
require_once "admin/config.php";
global $table;
$database = new database();

$pkid = mysql_real_escape_string($_GET['id']);

if ($_POST['submit_application']) {
    $postfields = $_POST;
    unset($postfields['submit_application']);
    unset($postfields['pdpa']);

    $postfields['created_date']=date("Y-m-d H:i:s");

    $query=get_query_insert($table['credit_card_enquiry'],$postfields);
    $database->query($query);

    send_email("*",$postfields);

    echo '<script>alert("Successfully Submitted!");</script>';
}

$result = get_query_data($table['credit_card'], "1 and pkid=$pkid");
$rs_array = $result->fetchRow();
?>
<!DOCTYPE html>
<html>
<?php include('head.php') ?>

<body class="inner">

<div class="header">
    <?php include('header.php') ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <div class="img-holder">
                    <img src="files/credit_card/<?= $rs_array['img_url'] ?>"
                    />
                </div>
            </div>
            <div class="col-md-9 col-sm-8">
                <h2 class="hero"><?= $rs_array['title'] ?></h2>
                <p>
                    <?= $rs_array['description'] ?>
                </p>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <div class="highlight-box">
        <div class="quick-feature">
            <div class="row">
                <div class="col-sm-3">
                    <div class="main-feature">
                        <label>Min. Monthly Income</label>
                        <figure>
                            <sup>RM</sup> <?= number_format($rs_array['income']) ?>
                        </figure>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="main-feature">
                        <label>Interest Rate</label>
                        <figure>
                            <?= $rs_array['interest'] ?>% <sup>p.a.</sup>
                        </figure>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="main-feature">
                        <label>Balance Transfer</label>
                        <figure>
                            <?= $rs_array['balance'] ?>% <sup>p.a.</sup>
                        </figure>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="main-feature">
                        <label>Annual Fee</label>
                        <figure>
                            <sup>RM</sup> <?= number_format($rs_array['annual']) ?>
                        </figure>
                    </div>
                </div>
            </div>
        </div>

        <div class="quick-review">
            <div class="row">
                <div class="col-md-4 col-sm-6 highlight-points">
                    <h3>Highlights</h3>
                    <ul class="styled-list">
                        <?
                        $array_db = explode("@|@", $rs_array['feature']);
                        $array_db = array_filter($array_db);

                        foreach ($array_db as $k => $v) {
                            ?>
                            <li>
                                <?= $v ?>
                            </li>
                        <? } ?>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6">
                    <ul class="list-unstyled scores">
                        <?
                        if ($rs_array['rating_petrol'] != "") {
                            ?>
                            <li>
                                <label>Petrol</label>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= $rs_array['rating_petrol'] * 10 ?>%"></div>
                                </div>
                                <figure>
                                    <?= $rs_array['rating_petrol'] ?> / 10
                                </figure>
                            </li>
                        <? } ?>

                        <?
                        if ($rs_array['rating_grocer'] != "") {
                            ?>
                            <li>
                                <label>Groceries</label>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= $rs_array['rating_grocer'] * 10 ?>%"></div>
                                </div>
                                <figure>
                                    <?= $rs_array['rating_grocer'] ?> / 10
                                </figure>
                            </li>
                        <? } ?>

                        <?
                        if ($rs_array['rating_insurance'] != "") {
                            ?>
                            <li>
                                <label>Insurance / Health</label>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= $rs_array['rating_insurance'] * 10 ?>%"></div>
                                </div>
                                <figure>
                                    <?= $rs_array['rating_insurance'] ?> / 10
                                </figure>
                            </li>
                        <? } ?>

                        <?
                        if ($rs_array['rating_utility'] != "") {
                            ?>
                            <li>
                                <label>Utilities</label>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= $rs_array['rating_utility'] * 10 ?>%"></div>
                                </div>
                                <figure>
                                    <?= $rs_array['rating_utility'] ?> / 10
                                </figure>
                            </li>
                        <? } ?>

                        <?
                        if ($rs_array['rating_shopping'] != "") {
                            ?>
                            <li>
                                <label>Shopping</label>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= $rs_array['rating_shopping'] * 10 ?>%"></div>
                                </div>
                                <figure>
                                    <?= $rs_array['rating_shopping'] ?> / 10
                                </figure>
                            </li>
                        <? } ?>

                        <?
                        if ($rs_array['rating_entertainment'] != "") {
                            ?>
                            <li>
                                <label>Entertainment</label>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= $rs_array['rating_entertainment'] * 10 ?>%"></div>
                                </div>
                                <figure>
                                    <?= $rs_array['rating_entertainment'] ?> / 10
                                </figure>
                            </li>
                        <? } ?>

                        <?
                        if ($rs_array['rating_travel'] != "") {
                            ?>
                            <li>
                                <label>Travel</label>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= $rs_array['rating_travel'] * 10 ?>%"></div>
                                </div>
                                <figure>
                                    <?= $rs_array['rating_travel'] ?> / 10
                                </figure>
                            </li>
                        <? } ?>

                        <?
                        if ($rs_array['rating_dining'] != "") {
                            ?>
                            <li>
                                <label>Dining</label>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width: <?= $rs_array['rating_dining'] * 10 ?>%"></div>
                                </div>
                                <figure>
                                    <?= $rs_array['rating_dining'] ?> / 10
                                </figure>
                            </li>
                        <? } ?>

                    </ul>
                </div>

                <div class="col-sm-12 text-center col-md-2">
                    <div class="main-cta">
                        <button type="button" class="btn btn-lg btn-icon" onclick="modal_application('<?=$rs_array['title']?>')"><i class="fa fa-wpforms"></i> Apply Now
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <section class="information-wrapper">
        <div class="information-section">
            <div class="row">
                <?
                $array_db = explode("@|@", $rs_array['content_petrol']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>Petrol</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>

                <?
                $array_db = explode("@|@", $rs_array['content_grocer']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>Groceries</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>

                <?
                $array_db = explode("@|@", $rs_array['content_insurance']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>Insurance / Health</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>

                <?
                $array_db = explode("@|@", $rs_array['content_utility']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>Utilities</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>

                <?
                $array_db = explode("@|@", $rs_array['content_shopping']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>Shopping</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>

                <?
                $array_db = explode("@|@", $rs_array['content_entertainment']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>Entertainment</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>

                <?
                $array_db = explode("@|@", $rs_array['content_travel']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>Travel</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>

                <?
                $array_db = explode("@|@", $rs_array['content_dining']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>Dining</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>

            </div>
        </div>

        <div class="information-section">
            <h2>Information</h2>

            <table class="table table-hover table-bordered">
                <tr>
                    <th class="col-xs-5">
                        Annual Fee
                    </th>
                    <td class="col-xs-7">
                        RM<?= number_format($rs_array['annual']) ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Min. Monthly Income
                    </th>
                    <td>
                        RM<?= number_format($rs_array['income']) ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Min. age for principal holder
                    </th>
                    <td>
                        <?= $rs_array['info_age1'] ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Min. age for supplementary holder
                    </th>
                    <td>
                        <?= $rs_array['info_age2'] ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Fee for supplementary holder
                    </th>
                    <td>
                        <?= $rs_array['info_fee'] ?>
                    </td>
                </tr>
                <tr>
                    <th rowspan="3">Finance charges</th>
                    <td>
                        <?= $rs_array['info_finance1'] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= $rs_array['info_finance2'] ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= $rs_array['info_finance3'] ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Goods & Services Tax (GST)
                    </th>
                    <td>
                        <?= $rs_array['info_gst'] ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Late payment charge
                    </th>
                    <td>
                        <?= $rs_array['info_late_payment'] ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Cash advance amount
                    </th>
                    <td>
                        <?= $rs_array['info_cash_advanced_amount'] ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Cash advance charge
                    </th>
                    <td>
                        <?= $rs_array['info_cash_advanced_charge'] ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Cash advance interest charge
                    </th>
                    <td>
                        <?= $rs_array['info_cash_advanced_interest'] ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Cashback Limit
                    </th>
                    <td>
                        <?= $rs_array['info_cashback'] ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Rewards points expiry
                    </th>
                    <td>
                        <?= $rs_array['info_reward_point'] ?>
                    </td>
                </tr>
            </table>
        </div>
    </section>
</div>

</body>


<?php include('footer.php') ?>
<?php include('js.php') ?>
</html>
