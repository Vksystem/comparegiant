<?php
require_once "admin/config.php";
global $table;
$database = new database();

if (isset($_POST['submit_subcat'])) {
    $search_category = mysql_real_escape_string($_POST['search-category']);
    $sub_category = mysql_real_escape_string($_POST['sub-category']);

    switch ($search_category) {
        case 'credit-card':
            header("Location: credit-card-listing.php?sc=" . $sub_category);
            break;
        case 'phone-plan':
            header("Location: phone-listing.php?sc=" . $sub_category);
            break;
        case 'broadband':
            header("Location: broadband-listing.php?sc=" . $sub_category);
            break;
        case 'postpaid':
            header("Location: postpaid-listing.php?sc=" . $sub_category);
            break;
        case 'insurance':
            header("Location: insurance-listing.php?sc=" . $sub_category);
            break;
    }
    exit();
}
?>
<!DOCTYPE html>
<html>
<?php include('head.php') ?>

<body id="index">

<div class="header">
    <?php include('header.php') ?>

    <div class="container">
        <h2 class="hero">I'm looking for the best:</h2>

        <form id="main-search" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
            <div class="form-group category-options">
                <label class="radio-inline checked">
                    <input type="radio" name="search-category" id="credit-card" value="credit-card" checked>
                    <div class="icon-holder">
                        <img src="img/creditcard.png" alt="Credit Card"/>
                    </div>
                    Credit Card
                </label>
                <label class="radio-inline">
                    <input type="radio" name="search-category" id="phone-plan" value="phone-plan">
                    <div class="icon-holder">
                        <img src="img/phone.png" alt="Smart Phone Plan"/>
                    </div>
                    Smart Phone Plan
                </label>
                <label class="radio-inline">
                    <input type="radio" name="search-category" id="broadband" value="broadband">
                    <div class="icon-holder">
                        <img src="img/broadband.png" alt="Broadband"/>
                    </div>
                    Broadband
                </label>
                <label class="radio-inline">
                    <input type="radio" name="search-category" id="mobile-plan" value="postpaid">
                    <div class="icon-holder">
                        <img src="img/sim.png" alt="Mobile Plan"/>
                    </div>
                    Mobile Plan
                </label>
                <!--<label class="radio-inline">
                    <input type="radio" name="search-category" id="insurance" value="insurance">
                    <div class="icon-holder">
                        <img src="img/umbrella.png" alt="Insurance"/>
                    </div>
                    Insurance
                </label>-->
            </div>

            <div class="separator-text">
                <span>with</span>
            </div>

            <div class="form-group">
                <select class="form-control selectpicker" id="sub-category" name="sub-category">
                    <?
                    $resultSubCat = get_query_data($table['credit_card_category'], 'status=1 order by sort_order asc');
                    while ($rs_subcat = $resultSubCat->fetchRow()) {
                        echo '<option value="' . $rs_subcat['pkid'] . '">' . $rs_subcat['title'] . '</option>';
                    }
                    ?>
                </select>
            </div>

            <hr class="sm-space"/>

            <button type="submit" name="submit_subcat" value="true" class="btn btn-lg btn-icon"><i
                        class="fa fa-search"></i> Compare
            </button>
        </form>

    </div>
</div>

</body>


<?php include('footer.php') ?>
<?php include('js.php') ?>
<script>
    $("input[name='search-category']").on('click', function () {
        $("#sub-category").load('view/index-sub-category.php?cat=' + this.value, function () {
            $(".selectpicker").selectpicker('refresh');
        });
    });
</script>
</html>
