<?php
require_once "admin/config.php";
global $table;
$database = new database();

$pkid = mysql_real_escape_string($_GET['id']);
$device_id = mysql_real_escape_string($_GET['did']);

if ($_POST['submit_application']) {
    $postfields = $_POST;
    unset($postfields['submit_application']);
    unset($postfields['pdpa']);

    $postfields['created_date'] = date("Y-m-d H:i:s");

    $query = get_query_insert($table['phone_enquiry'], $postfields);
    $database->query($query);

    send_email("*",$postfields);

    echo '<script>alert("Successfully Submitted!");</script>';
}

$result = get_query_data($table['phone_plan'], "1 and pkid=$pkid");
$rs_array = $result->fetchRow();

$resultDevice=get_query_data($table['phone_device'],"pkid=$device_id");
$rs_device=$resultDevice->fetchRow();

$resultPrice = get_query_data($table['device_to_plan'], "device_id=$device_id and plan_id=$pkid");
$rs_price = $resultPrice->fetchRow();
?>
<!DOCTYPE html>
<html>
<?php include('head.php') ?>

<body class="inner">

<div class="header">
    <?php include('header.php') ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <div class="img-holder">
                    <img src="files/phone_plan/<?= $rs_array['img_url'] ?>"
                    />
                </div>
            </div>
            <div class="col-md-9 col-sm-8">
                <h2 class="hero"><?= $rs_array['title']." & ".$rs_device['title'] ?></h2>
                <p>
                    <?= $rs_array['description'] ?>
                </p>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <div class="highlight-box">
        <div class="quick-feature">
            <div class="row">
                <div class="col-sm-3">
                    <div class="main-feature">
                        <label>Monthly Fee</label>
                        <figure>
                            <sup>RM</sup> <?= number_format($rs_array['monthly_fee']) ?>
                        </figure>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="main-feature">
                        <label>Monthly Data</label>
                        <figure>
                            <sup>RM</sup> <?= number_format($rs_array['data_quota']) ?>
                        </figure>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="main-feature">
                        <label>Upfront Fee</label>
                        <figure>
                            <sup>RM</sup> <?= number_format($rs_array['upfront_fee']) ?>
                        </figure>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="main-feature">
                        <label>Device Price</label>
                        <figure>
                            <sup>RM</sup> <?= number_format($rs_price['price']) ?>
                        </figure>
                    </div>
                </div>
            </div>
        </div>

        <div class="quick-review">
            <div class="row">
              <div class="col-sm-12 text-center">
                  <div class="main-cta">
                    <button type="button" class="btn btn-lg btn-icon" onclick="modal_application('<?= $rs_array['title']." & ".$rs_device['title'] ?>')">
                        <i class="fa fa-wpforms"></i> Apply Now
                    </button>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>

<div class="container">


    <section class="information-wrapper">
        <div class="information-section">
            <div class="row">
                <?
                $array_db = explode("@|@", $rs_array['content_call']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>Calls</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>

                <?
                $array_db = explode("@|@", $rs_array['content_sms']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>SMS</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>

                <?
                $array_db = explode("@|@", $rs_array['content_data']);
                $array_db = array_filter($array_db);

                if (count($array_db) > 0) {
                    ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="feature-listing">
                            <h3>Data Quota</h3>
                            <ul class=" styled-list">
                                <?
                                foreach ($array_db as $k => $v) {
                                    ?>
                                    <li><?= $v ?></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>
        <div class="information-section">
            <h2>Others Information</h2>

            <?=$rs_array['content_other']?>
        </div>
    </section>
</div>

</body>


<?php include('footer.php') ?>
<?php include('js.php') ?>
</html>
